import { Box } from "@/ui/design-system/box/box";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import Link from "next/link";
import { Container } from "@/ui/components/container/container"
import { LoginForm } from "./login.form";
import { FormType } from "@/types/form";

interface Props {
    form: FormType;
}


export const LoginView = ({form}:Props) => {
    return (
        <Container className="grid grid-cols-2 gap-20 mb-32 items-center">
            <div className="flex items-center">
                <div className="relative w-full h-[531px]">
                    <Image
                        fill
                        src='/assets/svg/auth-login.svg'
                        alt="illustration inscription"
                    />
                </div>
            </div>
            <div>
                <Box padding_y="py-5">
                    <div className="flex items-center justify-between">
                        <Typo variant="h5" component="h1" > Connexion</Typo>
                        <div className="flex justify-between items-center gap-2">
                            <Typo variant="caption-4" component="h2" theme="gray" > 
                                Tu n'as pas encore de compte?
                            </Typo>
                            <Typo variant="caption-4" component="h2" theme="primary" > 
                                <Link href="/connexion/inscription" >S'inscrire</Link>
                            </Typo>
                        </div>
                    </div>
                    <LoginForm form={form} />

                    <Typo variant="caption-4" theme="primary" className="max-w-md mx-auto text-center">
                        <div>
                            <Link href="/connexion/mots-de-passe-perdu">mot de passe perdu ?</Link>
                        </div>
                    </Typo>
                </Box>
            </div>
        </Container>
    );
}