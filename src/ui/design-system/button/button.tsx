import { IconProps } from "@/types/iconProps";
import clsx from "clsx";
import { Spinner } from "../spinner/spinner";
import { LinkType } from "@/lib/link-type";
import Link from "next/link";

interface Props {
    variant?: 'accent' | 'secondary' | 'outline' | 'disabled' | 'ico';
    size?: 'small' | 'medium' | 'large';
    icon?: IconProps;
    iconTheme?: 'accent' | 'secondary' | 'gray';
    iconPosition?: 'left' | 'right';
    disabled?: boolean;
    isLoading?: boolean;
    children?: React.ReactNode;
    baseUrl?: string,
    linkType?: LinkType,
    action?: Function,
    type?: "button" | "submit",
    fullwidth?: boolean
}

export const Button = ({
    variant = 'accent',
    size = 'medium',
    icon,
    iconTheme = 'accent',
    iconPosition = 'right',
    disabled,
    isLoading,
    children,
    baseUrl,
    linkType = LinkType.INTERNAL,
    action = () => {},
    type = "button",
    fullwidth = false

}: Props)=>{

    let variantStyles: string = '', 
        sizeStyles: string = '', 
        icoSize: number = 0;

    switch (variant) {
        case 'accent': //default
            variantStyles = "bg-primary text-white hover:bg-primary-400 rounded";
            break;
        case 'secondary':
            variantStyles = 'bg-primary-200 hover:bg-primary-300/50 text-primary rounded';
            break; 
        case 'outline':
            variantStyles = 'bg-white hover:bg-gray-400/50 border border-gray-500 text-gray-900 rounded';
            break;
        case 'disabled':
            variantStyles = 'bg-gray-400 border border-gray-500 text-gray-600 rounded cursor-not-allowed';
            break;
        case 'ico':
            if (iconTheme == 'accent') {
                variantStyles = 'bg-primary text-white hover:bg-primary-400 rounded-full'
            }
            if (iconTheme == 'secondary') {
                variantStyles = 'bg-primary-200 hover:bg-primary-300/50 text-primary rounded-full'
            }
            if (iconTheme == 'gray') {
                variantStyles = 'bg-gray-800 hover:bg-gray-600 text-white rounded-full'
            }
            break;
    }

    switch (size) {
        case 'small': 
            sizeStyles = `text-caption3 font-medium ${
                variant === 'ico' ? 'flex items-center justify-center w-[40px] h-[40px]':'px-[14px] py-[12px]'
            }`;
            icoSize = 18;
            break;
        case 'medium': //default
            sizeStyles = `text-caption2 font-medium ${
                variant === 'ico' ? 'flex items-center justify-center w-[50px] h-[50px]':'px-[18px] py-[15px]'
            }`;
            icoSize = 20;
            break;
        case 'large':
            sizeStyles = `text-caption1 font-medium ${
                variant === 'ico' ? 'flex items-center justify-center w-[60px] h-[60px]':'px-[22px] py-[18px]'
            }`;
            icoSize = 24;
            break;
    }

    const handleClick = () => {
        if(action) {
            action();
        }
    }

    const buttonContent = (
        <>
            {/* Si le bouton est en isLoading */}
            {isLoading && (
                <div className="absolute inset-0 flex items-center justify-center">
                    {/* Si variant est égale à accent mettre le spinner en blanc sinon laisser la couleur par défaut */}
                    {variant == 'accent' || variant == 'ico' ? (<Spinner size="small" variant="white" />) : (<Spinner size="small" />)}
                    
                </div>
            )}
            {/* Si icon existe et que la variante est définie sur ico */}
            <div className={clsx(isLoading && 'invisible')}>
                {icon && variant === 'ico' ? (<icon.icon size={icoSize} />
                ) : (
                    <div className={clsx(icon && 'flex item-center gap-1')}>
                        {icon && iconPosition === "left" && (
                            <icon.icon size={icoSize} /> 
                        )}
                        {children}
                        {icon && iconPosition === "right" && (
                            <icon.icon size={icoSize} /> 
                        )}
                    </div>
                )}
            </div>
        </>
    );

    const buttonElement = (
        <>
            <button
                type={type}
                className={clsx(variantStyles, sizeStyles, icoSize, fullwidth && 'w-full' , isLoading && 'cursor-not-allowed', 'relative', 'animate')}
                onClick={handleClick}
                disabled={ disabled || isLoading ? true : false }
            >
                {buttonContent}
            </button>
        </>
    );

    // Gestion du button avec les url et les actions
    if(baseUrl){
        if(linkType === LinkType.EXTERNAL){
            return (
                <a href={baseUrl} target="_blank"> {buttonElement} </a>
            );
        } else {
            return <Link href={baseUrl}> {buttonElement} </Link>
        }
    }

    return buttonElement
}