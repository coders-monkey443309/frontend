import { IconType } from "react-icons";

export interface IconProps {
    icon: IconType; //iconType est le type que fournit react-icon
}