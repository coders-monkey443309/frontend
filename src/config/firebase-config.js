import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";


const firebaseConfig = {
  apiKey: "AIzaSyAIAHkbhnxis6LDM2eqqC-we6_XFGLJArI",
  authDomain: "join-my-team.firebaseapp.com",
  projectId: "join-my-team",
  storageBucket: "join-my-team.appspot.com",
  messagingSenderId: "211872779390",
  appId: "1:211872779390:web:bd6d21d98ee5c3a8d0dc2f"
};

export const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);
