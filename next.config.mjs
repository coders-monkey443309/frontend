/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  output: 'export',
  images: {
    unoptimized: true //pour fixer les problèmes d'images
  }
};

export default nextConfig;
