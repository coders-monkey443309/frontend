import { FormType } from "@/types/form";
import { Button } from "@/ui/design-system/button/button";
import { Input } from "@/ui/design-system/forms/input";

interface Props {
    form: FormType
}

export const ForgetPasswordForm = ({form}: Props) => {

    // On fait un destructuring pour éviter de faire form.control par exemple
    const {
        control,
        onSubmit,
        errors,
        isLoading,
        register,
        handleSubmit} = form;
    // console.log('form', form);
    return (
        <form onSubmit={handleSubmit(onSubmit)} className="space-y-3 my-5">
            <Input
                type="email"
                placeholder="test@test.com"
                isLoading = {isLoading}
                register = {register}
                errors = {errors}
                errorMsg = 'Veuillez saisir votre e-mail'
                id = 'email'
            />
            <Button type="submit" isLoading={isLoading} fullwidth>Envoyer</Button>
        </form>
    );
}