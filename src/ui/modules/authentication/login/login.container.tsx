import { LoginFormFieldsType } from "@/types/form";
import { useEffect, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { LoginView } from "./login.view";

import { onAuthStateChanged } from "firebase/auth";
import { auth } from "@/config/firebase-config";
import { useToggle } from "@/hooks/use-toggle";
import { firebaseSignInUser } from "@/api/authentication";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

export const LoginContainer = () => {

    const {value: isLoading, setValue: setIsLoading} = useToggle();
    const router = useRouter();

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                const uid = user.uid;
                console.log('user', user)
            } else {
                console.log("pas connecté");
                
            }
        });
    }, []);

    const {
        handleSubmit,
        control,
        formState: { errors },
        register,
        setError,
        reset
    } = useForm<LoginFormFieldsType>();

    // Fonction Login
    const handleSignInUser = async({email, password}: LoginFormFieldsType) => {
        const { error } = await firebaseSignInUser(email, password);

        if(error) {
            setIsLoading(false);
            toast.error(error.message);
            return;
        }

        toast.success('Bienvenu sur notre plateforme');
        setIsLoading(false);
        reset();
        router.push('/mon-espace/')
    }

    // SubmitHandler vient de reactHookForm
    const onSubmit: SubmitHandler<LoginFormFieldsType> = async (formData) => {
        setIsLoading(true);
        const { password } = formData;
        if (password.length <= 5) {
            setError("password", {
                type: "manual",
                message: "Ton mot de passe soit avoir au minimum 6 caractères"
            })
            setIsLoading(false)
            return;
        }
        // console.log('formData', formData);
        handleSignInUser(formData)
    }

    return (
        <>
            <LoginView
                form = {{
                    errors,
                    control,
                    register,
                    handleSubmit,
                    onSubmit, 
                    isLoading
                }}
            />
        </>
    );
}