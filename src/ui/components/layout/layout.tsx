import { Footer } from "../navigation/footer";
import { Navigation } from "../navigation/navigation";
import { Breadcrumbs } from "../breadcrumbs/breadcrumbs";

interface Props {
    children: React.ReactNode;
    displayBreadcrumbs?: boolean
}

export const Layout = ({children, displayBreadcrumbs= true}: Props) => {
    return (
        <>
            <Navigation />
            {displayBreadcrumbs && <Breadcrumbs />}
            {children}
            <Footer />
        </>
    );
}