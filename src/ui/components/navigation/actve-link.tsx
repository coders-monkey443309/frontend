import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/router";
import { useMemo } from "react";

interface Props {
    href: string;
    children: React.ReactNode
}

export const ActiveLink = ({href, children}:Props) => {

    const router = useRouter();

    //useMemo Hook react pour memoriser comme le useEffect
    const isActif: boolean = useMemo(()=>{
        return router.pathname === href
    },[router.pathname, href])

    return (
        <Link href={href} className={clsx(isActif && 'text-primary font-medium')}>
            {children}
        </Link>
    );
}