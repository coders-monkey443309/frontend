import clsx from "clsx";
import { Typo } from "../typography/typography";

interface Props {
    isLoading: boolean,
    placeholder: string,
    type?: 'text' | 'email' | 'password',
    register: any,
    errors: any,
    errorMsg?: string,
    id: string,
    required?: boolean,
    isAutocompleted?: boolean,
}

export const Input = ({
    isLoading,
    placeholder,
    type = "text",
    register,
    errors,
    errorMsg = 'Ce champs est obligatoire',
    id,
    required = true,
    isAutocompleted = false,
}: Props) => {
    // Gestion des erreurs
    console.log('errors', errors[id])
    return (
        <div>
            <input 
                type={type} 
                placeholder={placeholder} 
                className={clsx(
                    isLoading && 'cursor-not-allowed',
                    errors[id] ? 'placeholder-alert-danger text-alert-danger' : 'placeholder-gray-600',
                    "w-full p-4 font-light border border-gray-400 rounded focus:outline-none focus:ring-1 focus:ring-primary"
                )} 
                disabled={isLoading}
                {...register(id, {
                    required: {
                        value: required,
                        message: errorMsg
                    },
                })}
                autoComplete={isAutocompleted ? 'on' : 'off'}
            />
            {errors[id] && (
                <Typo variant="caption-4" component="span" theme="danger">
                    {errors[id]?.message}
                </Typo>
            )}
        </div>
    );
}