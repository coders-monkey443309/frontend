import { Seo } from "@/ui/components/seo/seo";
import { Layout } from '@/ui/components/layout/layout';
import { UserAccountContainer } from "@/ui/modules/user-profile/user-account.container";

export default function MonEspace() {
    return (
        <>
            <Seo title="Mon espace" description="Page mon compte" />
            <Layout>
                <UserAccountContainer />
            </Layout>
        </>
    );
}
