import { Typo } from "@/ui/design-system/typography/typography";
import { Container } from "../container/container";
import Image from "next/image";
import { footerLinks } from "./app-links";
import { v4 as uudiv4 } from "uuid";
import { ActiveLink } from "./actve-link";
import { FooterLinks } from "@/types/app-links";
import { LinkType } from "@/lib/link-type";
import { SocialNetworksBtn } from "./social-networks-btn";

// interface Props {}

export const Footer = () => {

    const currentYear = new Date().getFullYear();

    const footerNavigationList = footerLinks.map((footerLink)=>(
            <FooterLink key={uudiv4()} data={footerLink} />
    ));

    return (
        <div className="bg-gray">
            <Container className="flex justify-between pt-16">
                <div className="flex flex-col gap-1 items-center">
                    <Typo variant="caption-1" theme="white" weight="medium">Formations gratuites</Typo>
                    <Typo variant="caption-3" theme="gray">Abonne toi</Typo>
                    <a href="#" target="_blank">
                        <Image 
                            src='/assets/svg/YTB.svg' 
                            alt="youtube" 
                            width={229}
                            height={216}
                        />
                    </a>
                </div>
                <div className="flex gap-7">
                    {footerNavigationList}
                </div>
            </Container>
            <Container className="space-y-11 py-10">
                <hr className="text-gray-800" />
                <div className="flex items-center justify-between">
                    <Typo theme="gray" variant="caption-4" weight="medium">Copyright © {currentYear} | Propulsed by Kani Bouebassihou - kboueb on github</Typo>
                    <div>
                        <SocialNetworksBtn theme='gray'/>
                    </div>
                </div>
            </Container>
        </div>
    );
}

interface FooterLinkProps {
    data: FooterLinks;
}

const FooterLink = ({data}:FooterLinkProps) =>{

    const listLinks = data.links.map((link)=>(
        <div key={uudiv4()}>
            {link.type === LinkType.INTERNAL && (<ActiveLink href={link.baseUrl} > {link.label} </ActiveLink>)}
            {link.type === LinkType.EXTERNAL && (<a href={link.baseUrl} target="_blank"> {link.label} </a>)} 
        </div>
    ));

    return (
        <div className="min-w-[190px]">
            <Typo 
                theme="white"
                variant="caption-2"
                weight="medium"
                className="pb-5"
            >
                {data.label}
            </Typo>
            <Typo theme="gray" variant="caption-3" className="space-y-4" >
                {listLinks}
            </Typo>
        </div>
    );
}