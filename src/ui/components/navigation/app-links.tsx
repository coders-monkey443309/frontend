import { AppLinks } from "@/types/app-links"
import { RiLinkedinFill, RiSlackFill, RiYoutubeFill } from "react-icons/ri"

export const footerAppLinks: AppLinks[] = [
    {
        label: 'Accueil',
        baseUrl: '/',
        type: 'internal',
    },
    {
        label: 'Projets',
        baseUrl: '/#',
        type: 'internal',
    },
    {
        label: 'Coders Monkeys',
        baseUrl: '/#',
        type: 'internal',
    },
    {
        label: 'Formations',
        baseUrl: 'https://youtube.com/@remotemonkey',
        type: 'external',
    },
]

export const footerUserLinks: AppLinks[] = [
    {
        label: 'Mon espace',
        baseUrl: '/#',
        type: 'internal',
    },
    {
        label: 'Connexion',
        baseUrl: '/connexion',
        type: 'internal',
    },
    {
        label: 'Inscription',
        baseUrl: '/connexion/inscription',
        type: 'internal',
    },
    {
        label: 'Mot de passe oublié',
        baseUrl: '/connexion/mots-de-passe-perdu',
        type: 'internal',
    },
]

export const footerInfosLinks: AppLinks[] = [
    {
        label: 'CGU',
        baseUrl: '/#',
        type: 'internal',
    },
    {
        label: 'Confidentialité',
        baseUrl: '/#',
        type: 'internal',
    },
    {
        label: 'A propos',
        baseUrl: '/#',
        type: 'internal',
    },
    {
        label: 'Contact',
        baseUrl: '/#',
        type: 'internal',
    },
]

export const footerRsLinks: AppLinks[] = [
    {
        label: 'LinkedIn',
        baseUrl: 'https://www.linkedin.com/in/kani-bouebassihou-543b87180/',
        type: 'external',
        icon: RiLinkedinFill,
    },
    {
        label: 'Youtube',
        baseUrl: '/#',
        type: 'external',
        icon: RiYoutubeFill,

    },
    {
        label: 'Slack',
        baseUrl: '/#',
        type: 'external',
        icon: RiSlackFill,

    },
]

// Tableau pour regrouper les titres et les différentes données des composents footer
export const footerLinks = [
    {
        label: 'App',
        links: footerAppLinks,
    },
    {
        label: 'Utilisateurs',
        links: footerUserLinks,
    },
    {
        label: 'Informations',
        links: footerInfosLinks,
    },
    {
        label: 'Réseaux',
        links: footerRsLinks,
    },
]