// .container.tsx c'est pour la logique dev
// .view.tsx c'est pour la partie design

interface Props {
    
}

import { LandinPageView } from "./landing-page.view";

export const LandinPageContainer = () => {
    return (
        <>
            <LandinPageView />
        </>
    );
}