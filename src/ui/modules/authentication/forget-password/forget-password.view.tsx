import { Box } from "@/ui/design-system/box/box";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import Link from "next/link";
import { Container } from "@/ui/components/container/container"
import { ForgetPasswordForm } from "./forget.form";
import { FormType } from "@/types/form";

interface Props {
    form: FormType
}

export const ForgetPasswordView = ({form}: Props) => {
    return (
        <Container className="grid grid-cols-2 gap-20 mb-32 items-center">
            <div className="flex items-center">
                <div className="relative w-full h-[531px]">
                    <Image
                        fill
                        src='/assets/svg/auth-mp.svg'
                        alt="illustration inscription"
                    />
                </div>
            </div>
            <div>
                <Box padding_y="py-5">
                    <div className="flex items-center justify-between">
                        <Typo variant="h5" component="h1" > Mot de passe perdu?</Typo>
                        <Typo variant="caption-4" component="h2" theme="primary" > 
                            <Link href="/connexion" >Connexion</Link>
                        </Typo>
                    </div>
                    <ForgetPasswordForm form={form}/>
                </Box>
            </div>
        </Container>
    );
}