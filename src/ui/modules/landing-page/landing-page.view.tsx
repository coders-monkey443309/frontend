// .container.tsx c'est pour la logique dev
// .view.tsx c'est pour la partie design

import { Typo } from "@/ui/design-system/typography/typography";
import { HeroTopView } from "./components/hero-top/hero-top.view";
import { FeaturedView } from "./components/featured/featured.view";
import { SlackCTA } from "./components/slack-cta/slack-cta.view";
import { CurrentCourseCTA } from "./components/current-course-cta/current-course-cta.view";

export const LandinPageView = () => {
    return (
        <>
            <HeroTopView />
            <FeaturedView />
            <SlackCTA />
            <CurrentCourseCTA />
        </>
    );
}