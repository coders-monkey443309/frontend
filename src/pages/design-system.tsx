import { Navigation } from "@/ui/components/navigation/navigation";
import { Seo } from "@/ui/components/seo/seo";
import { Avatar } from "@/ui/design-system/avatar/avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/logo";
import { Spinner } from "@/ui/design-system/spinner/spinner";
import { Typo } from "@/ui/design-system/typography/typography";
import { RiAlarmWarningLine } from "react-icons/ri";
import { Container } from '../ui/components/container/container';
import { Footer } from "@/ui/components/navigation/footer";
import { Layout } from "@/ui/components/layout/layout";


export default function DesignSystem(){
    return (
        <>
            <Seo title="Home page" description="description..." />
            <Layout>
                <Container>
                    <Typo variant="h1" component="h1" weight="medium" className="p-5">Mon design system</Typo>

                    <div className="p-5 flex items-center gap-4">
                    <Avatar alt="Avatar du coder" size="small" source='/assets/images/daniel-lincoln-pe-X2NUwVQo-unsplash.png'/>
                    <Avatar alt="Avatar du coder" source='/assets/images/daniel-lincoln-pe-X2NUwVQo-unsplash.png'/>
                    <Avatar alt="Avatar du coder" size="large" source='/assets/images/daniel-lincoln-pe-X2NUwVQo-unsplash.png'/>
                    <Logo size="very-small" />
                    <Logo size="small"/>
                    <Logo />
                    <Logo size="large"/>
                    </div>

                    <div className="p-5 flex items-center gap-4">
                    <Spinner size="small" variant="primary"/>
                    <Spinner />
                    <Spinner size="large"/>
                    </div>

                    <div className="p-5 flex items-center gap-4">
                    <Button isLoading size="small">accent</Button>
                    <Button isLoading variant="secondary" size="small">secondary</Button>
                    <Button isLoading variant="outline" size="small">outline</Button>
                    <Button isLoading variant="disabled" size="small" disabled>disabled</Button>
                    <Button isLoading variant="ico" size="small" icon={{ icon: RiAlarmWarningLine }}/>

                    </div>

                    <div className="p-5 flex items-center gap-4">
                    <Button size="small">accent</Button>
                    <Button variant="secondary" size="small">secondary</Button>
                    <Button variant="outline" size="small">outline</Button>
                    <Button variant="disabled" size="small" disabled>disabled</Button>
                    <Button variant="ico" size="small" icon={{ icon: RiAlarmWarningLine }}/>
                    <Button variant="secondary" iconTheme="gray" size="small" iconPosition="left" icon={{ icon: RiAlarmWarningLine }}>Button test</Button>
                    </div>

                    <div className="p-5 flex items-center gap-4">
                    <Button>accent</Button>
                    <Button variant="secondary">secondary</Button>
                    <Button variant="outline">outline</Button>
                    <Button variant="disabled">disabled</Button>
                    <Button variant="ico" icon={{ icon: RiAlarmWarningLine }}/>
                    <Button variant="accent" iconTheme="secondary" icon={{ icon: RiAlarmWarningLine }}>Button Prod</Button>
                    <Button variant="secondary" iconTheme="gray" iconPosition="left" icon={{ icon: RiAlarmWarningLine }}>Button test</Button>
                    </div>

                    <div className="p-5 flex items-center gap-4">
                    <Button size="large">accent</Button>
                    <Button variant="secondary" size="large">secondary</Button>
                    <Button variant="outline" size="large">outline</Button>
                    <Button variant="disabled" size="large">disabled</Button>
                    <Button variant="ico" size="large" iconTheme="gray" icon={{ icon: RiAlarmWarningLine }}/>
                    <Button variant="ico" size="large" icon={{ icon: RiAlarmWarningLine }}/>
                    <Button variant="ico" size="large" iconTheme="secondary" icon={{ icon: RiAlarmWarningLine }}/>
                    <Button variant="accent" iconTheme="secondary" size="large" icon={{ icon: RiAlarmWarningLine }}>Button Prod</Button>
                    </div>

                    <div className="space-y-3">
                    <Typo variant="display" theme="secondary" component="h1">Display</Typo>
                    <Typo variant="h1" component="h1" theme="primary" weight="medium">H1</Typo>
                    <Typo variant="h4" component="h1" weight="medium">H4</Typo>
                    <Typo variant="body-lg" component="h1" weight="medium">Body-lg</Typo>
                    <Typo variant="caption-2" component="h1" weight="regular">Caption-2</Typo>
                    </div>
                </Container>
            </Layout>
            
            
        </>
    );
}