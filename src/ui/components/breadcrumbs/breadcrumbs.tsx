import { Typo } from "@/ui/design-system/typography/typography";
import clsx from "clsx";
import { useRouter } from "next/router";
import { RiHome3Line } from "react-icons/ri";
import { uuid } from "uuidv4";
import { Container } from "../container/container";
import Link from "next/link";

export const Breadcrumbs = () => {
    const router = useRouter();
    const asPath = router.asPath;
    const segments = asPath.split('/');
    const lastSegment = segments[segments.length - 1]
    segments[0] = "accueil";
    // console.log('segments',segments);
    
    const view = segments.map((path, index)=>(
        <div key={uuid()}>
            <Link href={
                index > 0 ? `/${segments.slice(1, index + 1).join("/")}` : '/'
            }>
                <Typo variant="caption-3" component="span" className={clsx(
                    path !== lastSegment ? 'text-gray-600' : 'text-gray',
                    "capitalize hover:text-gray animate cursor-pointer"
                )}>
                    {
                        path !== 'accueil' ? (
                            path.replace(/-/g,' ')
                        ) : <RiHome3Line className="inline -mt-1" />
                    }
                    
                </Typo>
                {
                    path !== lastSegment && (
                        <Typo variant="caption-2" component="span" className='ml-2 text-gray-600'>/</Typo>
                    )
                }
            </Link>
        </div>
    ));
    return (
        <Container className="flex items-center gap-2 py-7">
            {view}
        </Container>
    );
}