import { SubmitHandler, useForm } from "react-hook-form";
import { ForgetPasswordView } from "./forget-password.view";
import { ForgetPasswordFormFieldsType } from "@/types/form";
import { useToggle } from "@/hooks/use-toggle";
import { firebaseForgotPassword } from "@/api/authentication";
import { toast } from "react-toastify";

export const ForgetPasswordContainer = () => {
    const {value:isLoading, setValue:setIsLoading} = useToggle();

    const {
        handleSubmit,
        control,
        formState: { errors },
        register,
        setError,
        reset
    } = useForm<ForgetPasswordFormFieldsType>();

    // fonction pour l'authentification
    const handleSendForgetPassword = async ({
        email,
    }: ForgetPasswordFormFieldsType) => {
        const { error, data } = await firebaseForgotPassword(email);
        if (error) {
            setIsLoading(false);
            toast.error(error.message)
            return
        }

        toast.success('Un lien de réinitialisation a été envoyé sur votre mail');
        reset();
        setIsLoading(false);
    };

    // SubmitHandler vient de reactHookForm
    const onSubmit: SubmitHandler<ForgetPasswordFormFieldsType> = async (formData) => {
        setIsLoading(true);
        // console.log('formData', formData);
        handleSendForgetPassword(formData);
    }

    return (
        <>
            <ForgetPasswordView 
                form = {{
                    errors,
                    control,
                    register,
                    handleSubmit,
                    onSubmit, 
                    isLoading
                }}
            />
        </>
    );
}