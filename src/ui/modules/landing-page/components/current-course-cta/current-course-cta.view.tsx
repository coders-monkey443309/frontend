import { Container } from "@/ui/components/container/container";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import { RiPlayCircleFill } from "react-icons/ri";

export const CurrentCourseCTA = () => {
    return (
        <div className="bg-gray-300">
            <Container className="py-24 text-center">
                <Typo variant="h2" component="h2"className="mb-2.5" >
                    Formation Nextjs gratuite
                </Typo>
                <Typo variant="lead" component="h3"className="mb-5" >
                    Apprend à codeer l'app des singes coders
                </Typo>
                <Typo variant="caption-3" component="p" theme="gray" className="mb-16" >
                    Si tu veux un CV plus sexy que ton ex, suis cette formation complète
                </Typo>
                <a href="/#">
                    <div className="relative bg-gray-400 rounded h-[626px]">
                        <div className="flex flex-col items-center justify-center gap-4 relative h-full bg-gray z-10 rounded opacity-0 hover:opacity-90 animate text-white">
                            <RiPlayCircleFill size={42}/>
                            <Typo weight="medium" theme="white" variant="caption-2" className="uppercase">
                                Lire la formation
                            </Typo>
                        </div>
                        <Image fill src='/assets/images/cccta.png' alt="Image formation" className="object-cover object-center rounded" />
                    </div>
                </a>
            </Container>
        </div>
    );
}