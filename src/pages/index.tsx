import { Footer } from '@/ui/components/navigation/footer';
import { Container } from '../ui/components/container/container';
import { Navigation } from "@/ui/components/navigation/navigation";
import { Seo } from "@/ui/components/seo/seo";


// import { Avatar } from "@/ui/design-system/avatar/avatar";
// import { Button } from "@/ui/design-system/button/button";
// import { Logo } from "@/ui/design-system/logo/logo";
// import { Spinner } from "@/ui/design-system/spinner/spinner";
// import { Typo } from "@/ui/design-system/typography/typography";


import { Inter } from "next/font/google";
import { Layout } from '@/ui/components/layout/layout';
import { LandinPageContainer } from '@/ui/modules/landing-page/landing-page.container';
// import { RiAlarmWarningLine } from "react-icons/ri";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <>
      <Seo title="Homepage" description="Page d'accueil" />
      <Layout displayBreadcrumbs={false}>
        <LandinPageContainer />
      </Layout>
    </>
  );
}
