import { SubmitHandler, useForm } from "react-hook-form";
import { RegisterView } from "./register.view";
import { RegisterFormFieldsType } from "@/types/form";
import { firebaseCreateUser } from "@/api/authentication";

import { toast } from 'react-toastify';
import { useToggle } from "@/hooks/use-toggle";


export const RegisterContainer = () => {

    // const [isLoading, setIsLoading] = useState<boolean>(false);

    const {value:isLoading, setValue:setIsLoading} = useToggle();

    const {
        handleSubmit,
        control,
        formState: { errors },
        register,
        setError,
        reset
    } = useForm<RegisterFormFieldsType>();

    // fonction pour l'authentification
    const handleCreateUserAuthentication = async ({
        email, 
        password, 
        how_did_hear
    }: RegisterFormFieldsType) => {
        const { error, data } = await firebaseCreateUser(email, password);
        if (error) {
            setIsLoading(false);
            toast.error(error.message)
            return
        }

        toast.success('Bienvvenu sur notre plateforme');
        reset();
        setIsLoading(false);
    };

    // SubmitHandler vient de reactHookForm
    const onSubmit: SubmitHandler<RegisterFormFieldsType> = async (formData) => {
        setIsLoading(true);
        console.log('formData', formData);

        const { password } = formData;

        if(password.length <= 5) {
            setError("password", {
                type: "manual",
                message: "Ton mot de passe doit avoir au moins 6 caractères"
            });
            setIsLoading(false);
            // return pour éviter d'exécuter l'API quand on a une erreur
            return
        }

        handleCreateUserAuthentication(formData);
    }

    return (
        <>
            <RegisterView
                form = {{
                    errors,
                    control,
                    register,
                    handleSubmit,
                    onSubmit, 
                    isLoading
                }}
            />
        </>
    );
}