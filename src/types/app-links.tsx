import { LinkType } from "@/lib/link-type";
import { IconProps } from "./iconProps";
import React from "react";
import { IconType } from "react-icons";

export interface AppLinks {
    label: string;
    baseUrl: string;
    type: LinkType;
    icon?: IconType; //Type pour les icons
}

export interface FooterLinks {
    label: string,
    links: AppLinks[]
}