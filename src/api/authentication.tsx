import { auth } from "@/config/firebase-config"
import { FirebaseError } from "firebase/app";
import { createUserWithEmailAndPassword, sendPasswordResetEmail, signInWithEmailAndPassword, signOut } from "firebase/auth"

export const firebaseCreateUser = async (email: string, password: string) => {
    try {
        const userCredential = await createUserWithEmailAndPassword(auth, email, password);
        return { data: userCredential.user }
    } catch (error) {

        const firebaseError = error as FirebaseError;
        return { error: {
            code: firebaseError.code,
            message: firebaseError.message
        }}
    }
}

// Fonction pour le login
export const firebaseSignInUser = async (email: string, password: string) => {
    try {
        const userCredential = await signInWithEmailAndPassword(auth, email, password);
        return { data: userCredential.user }
    } catch (error) {

        const firebaseError = error as FirebaseError;
        return { error: {
            code: firebaseError.code,
            message: firebaseError.message
        }}
    }
}

// Fonction pour le logout
export const firebaseLogoutUser = async () => {
    try {
        await signOut(auth);
        return { data: true }
    } catch (error) {

        const firebaseError = error as FirebaseError;
        return { error: {
            code: firebaseError.code,
            message: firebaseError.message
        }}
    }
}

// Fonction pour le mot de passe oublié
export const firebaseForgotPassword = async (email: string) =>{
    try {
        const userCredential = await sendPasswordResetEmail(auth, email);
        return {data : true}
    } catch (error) {
        const firebaseError = error as FirebaseError;
        return { error: {
            code: firebaseError.code,
            message: firebaseError.message
        }}
    }

}