import { Container } from "@/ui/components/container/container"
import { Box } from "@/ui/design-system/box/box";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import Link from "next/link";
import { RegisterForm } from "./register.form";
import { FormType } from "@/types/form";

interface Props {
    form: FormType;
}

export const RegisterView = ({form}: Props) => {
    return (
        <Container className="grid grid-cols-2 gap-20 mb-32 items-center">
            <div className="flex items-center">
                <div className="relative w-full h-[531px]">
                    <Image
                        fill
                        src='/assets/svg/auth-register.svg'
                        alt="illustration inscription"
                    />
                </div>
            </div>
            <div>
                <Box padding_y="py-5">
                    <div className="flex items-center justify-between">
                        <Typo variant="h5" component="h1" > Inscription</Typo>
                        <div className="flex justify-between items-center gap-2">
                            <Typo variant="caption-4" component="h2" theme="gray" > 
                                Tu as déjà un compte?
                            </Typo>
                            <Typo variant="caption-4" component="h2" theme="primary" > 
                                <Link href="/connexion" >Connexion</Link>
                            </Typo>
                        </div>
                    </div>
                    <RegisterForm form={form} />
                    <Typo variant="caption-4" theme="gray" className="max-w-md mx-auto space-y-1 text-center">
                        <div>En t'inscrivant tu acceptes les</div>
                        <div>
                            <Link href="/#" className="text-gray">Conditions d'utilisations</Link>
                            {" "}et la {" "}
                            <Link href="/#" className="text-gray">Politique de Confidentialité</Link>
                        </div>
                    </Typo>
                </Box>
            </div>
        </Container>
    );
}