import { firebaseLogoutUser } from "@/api/authentication";
import { Button } from "@/ui/design-system/button/button";
import { useRouter } from "next/router";
import { toast } from "react-toastify";



export const UserAccountContainer = () => {

    const router = useRouter();
    const handleLogoutUser = async() => {
        const {error} = await firebaseLogoutUser();
        if(error) {
            toast.error(error.message);
            return;
        }
    
        toast.success('A bientôt!');
        router.push('/connexion/')
    
    }

    return (
        <>
            <div className="flex justify-center pt-20 pb-40">
                <Button action={handleLogoutUser} variant="secondary">Déconnexion</Button>
            </div>
        </>
    );
}