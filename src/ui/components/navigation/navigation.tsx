import { Logo } from "@/ui/design-system/logo/logo";
import { Container } from "../container/container";
import { Typo } from "@/ui/design-system/typography/typography";
import { Button } from "@/ui/design-system/button/button";
import { ActiveLink } from "./actve-link";

interface Props {}

export const Navigation = ({}:Props) => {
    return (
        <div className="border-b-2 border-gray-400">
            <Container className="flex item-center justify-between gap-7 py-1.5">
                <ActiveLink href="/">
                    <div className="flex items-center gap-2.5">
                        <Logo size="small" />
                        <div className="flex flex-col">
                            <div className="text-gray font-extrabold text-[24px]">
                                Coders Monkey
                            </div>
                            <Typo 
                                theme="gray" 
                                variant="caption-4"
                                component="span"
                            >
                                Trouve de l’inspiration & reçois des feedbacks !
                            </Typo>
                        </div>
                    </div>              
                </ActiveLink>
                <div className="flex items-center gap-7">
                    <Typo variant="caption-3" component="div" className="flex items-center gap-7">
                        <ActiveLink href="/design-system">Design System</ActiveLink>
                        <ActiveLink href="/projets">Projets</ActiveLink>
                        <ActiveLink href="/formations">Formations</ActiveLink>
                        <ActiveLink href="/contact">Contact</ActiveLink>
                    </Typo>
                    <div className="flex items-center gap-2">
                        <Button baseUrl="/connexion" size="small">Connexion</Button>
                        <Button baseUrl="/connexion/inscription" size="small" variant="secondary">Rejoindre</Button>
                    </div>
                </div>
            </Container>
        </div>
    );
}