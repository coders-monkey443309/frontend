import { Container } from "@/ui/components/container/container";
import { SocialNetworksBtn } from "@/ui/components/navigation/social-networks-btn";
import { Button } from "@/ui/design-system/button/button";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import { RiArrowRightLine } from "react-icons/ri";
import { uuid } from "uuidv4";

interface FeaturesListInterface {
    imageUrl: string,
    imageAlt: string,
    title: string,
    description: string
}

// FeaturesListInterface est l'interface de type de featuresData
const featuresData: FeaturesListInterface[] = [
    {
        imageUrl: '/assets/images/ressources.png',
        imageAlt: 'Ressources',
        title: 'Ressources',
        description: 'Consulte et partage des ressources pour les devs'
    },

    {
        imageUrl: '/assets/images/entrainement.png',
        imageAlt: 'Entrainement',
        title: 'Entrainement',
        description: 'Entraîne-toi à devenir meilleur et trouve de l’inspiration'
    },

    {
        imageUrl: '/assets/images/visibilité.png',
        imageAlt: 'Visibilité',
        title: 'Visibilité',
        description: 'Expose tes projets et crée toi des opportunités !'
    },

    {
        imageUrl: '/assets/images/relations.png',
        imageAlt: 'Relations',
        title: 'Relations',
        description: 'Connecte-toi avec des devs web et booste ta carrière !'
    },
]

export const FeaturedView = () => {

    const featuredList = featuresData.map((data)=>(
        <div className="flex flex-col items-center justify-center bg-white rounded p-5 space-y-5 text-center" key={uuid()}>
            <Image alt={data.imageAlt} src={data.imageUrl} width={130} height={130}/>
            <Typo variant="lead" weight="medium" component="h3" >{data.title}</Typo>
            <Typo variant="body-base" component="div" className="text-gray-700" >{data.description}</Typo>
        </div>
    ));

    return (
        <div className="bg-gray-300">
            <Container className="grid grid-cols-12 gap-20 py-24">
                <div className="grid grid-cols-2 gap-5 col-span-7">
                    {featuredList}
                </div>
                <div className="flex flex-col justify-between gap-10 col-span-5">
                    <div>
                        <Typo variant="h2" component="h2" weight="medium" className="mb-5">L’endroit le plus cool pour devenir développeur</Typo>
                        <Typo variant="body-lg" className="mb-8" theme="gray" component="div">Du partage, des connexions et des formations notre app gère tout ça pour toi. Rejoins la communauté et grimpe en grade. Let's go !</Typo>
                        <Button iconPosition="right" variant="secondary" baseUrl="/#" icon={{icon: RiArrowRightLine}} >Commencer</Button>
                    </div>
                    <div className="">
                        <Typo variant="caption-3" className="mb-4" theme="gray">Nos réseaux sociaux</Typo>
                        <SocialNetworksBtn />
                    </div>
                </div>
            </Container>
        </div>
    );
}