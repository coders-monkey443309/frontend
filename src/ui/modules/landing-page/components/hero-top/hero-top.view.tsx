import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";


export const HeroTopView = () => {
    return (
        <Container className="relative flex justify-between items-center py-20 overflow-hidden">
            <div className="space-y-7">
                <Typo variant="h1" component="h1" theme="black" weight="medium">Rejoins les singes codeurs !</Typo>
                <Typo variant="body-lg" theme="gray" >
                    Ici, on se prend pas la tête, mais on code comme des bêtes !           
                    Rejoins notre tribu de singes codeurs, partage tes projets les plus fous et fais-toi de nouveaux amis développeurs.
                </Typo>
                <div className="space-x-7 pt-2.5">
                    <Button >Commencer</Button>
                    <Button variant="secondary">En savoir plus</Button>
                </div>
            </div>
            <Image
                src='/assets/svg/rocket.svg'
                alt=""
                width={811}
                height={596}
            />
        </Container>
    );
}