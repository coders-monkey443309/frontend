import { Button } from "@/ui/design-system/button/button";
import { footerRsLinks } from "./app-links";
import { v4 as uudiv4 } from "uuid";
import { Ri4KFill, RiFacebookCircleFill } from "react-icons/ri";
import clsx from "clsx";

interface Props{
    className?: string
    theme?: 'gray' | 'accent' | 'secondary'
}


export const SocialNetworksBtn = ({
    className, 
    theme = 'accent'
}:Props) => {

    const icoList = footerRsLinks.map((socialNetwork)=>(
        <Button
            key={uudiv4()}
            variant="ico"
            iconTheme={theme}
            baseUrl={socialNetwork.baseUrl}
            linkType={socialNetwork.type}
            icon={{ 
                icon: socialNetwork.icon ? socialNetwork.icon : Ri4KFill 
                //ternaire pour eviter le undefined
            }}
        />
    ));

    return (
        <div className={clsx('flex items-center gap-2.5',className)}>
            {icoList}
        </div>
    );
}