import { LinkType } from "@/lib/link-type";
import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/logo";
import { Typo } from "@/ui/design-system/typography/typography";
import Image from "next/image";

export const SlackCTA = () => {
    return (
        <Container className="flex justify-between">
            <div className="flex flex-col justify-center max-w-2xl space-y-5">
                <div className="flex items-center justify-start gap-2">
                    <Logo size="very-small"/>
                    <Typo variant="caption-2">Coders Monkey</Typo>
                </div>
                <Typo component="h2" variant="h2">
                    Rejoins-nous sur le Slack
                    des Singes codeurs
                </Typo>
                <Typo component="p" variant="body-lg" theme="gray">
                    Rejoins-nous et obtiens de l’aide, des conseils et pourquoi pas des nouveaux potes !
                </Typo>
                <Button baseUrl="/#" linkType={LinkType.EXTERNAL}>Rejoindre le groupe d'aide</Button>
            </div>
            <div className="relative w-[600px] h-[600px]">
                <Image fill src="/assets/svg/logo-slack.svg" alt="icon group slack"/>
            </div>
        </Container>
    );
}