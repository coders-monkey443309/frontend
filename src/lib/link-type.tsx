export type LinkType = 'internal' | 'external' //type les données du linktype
// Création de notre première librarie
export const LinkType: Record<string, LinkType> = {
    INTERNAL : 'internal',
    EXTERNAL : 'external',
}